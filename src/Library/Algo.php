<?php

namespace KDA\Helpers\Library;

use KDA\Helpers\Facades\Luhn;
use KDA\Helpers\Facades\Modulo11;
use KDA\Helpers\Facades\Modulo10;

class Algo {
    
    public function modulo10($str){
        return Luhn::create($str);
    }
    public function modulo11($str){
        return Modulo11::calculateCheckDigit($str);
    }
    public function modulo10_verify($str){
        return Luhn::validate($str);
    }
    public function ean13($str){
        return Modulo10::calculateCheckDigit($str,[1,3]);
    }
    public function isbn10_to_isbn13($isbn,$isbn13_prefix='978'){
        $isbn = preg_replace("/[^0-9X]/","",$isbn);      
        if(strlen($isbn)==10){
            //remove mod10
            $isbn = substr($isbn,0,9);

            return $isbn13_prefix.$isbn.$this->ean13($isbn13_prefix.$isbn);
        }
        return false;
    }
    public function isbn13_to_isbn10($isbn){
        $isbn = preg_replace("/[^0-9]/","",$isbn);      
        if(strlen($isbn)==13){
            //remove mod10
            $isbn = substr($isbn,3,9);
           // dd($isbn);
            return $isbn.$this->modulo11($isbn);
        }
        return false;
    }



}