<?php 
namespace KDA\Helpers\Facades;

use Illuminate\Support\Facades\Facade;


class Modulo11 extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'modulo11'; }
}