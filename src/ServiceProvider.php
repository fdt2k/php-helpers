<?php

namespace KDA\Helpers;

use KDA\Laravel\PackageServiceProvider;
use Selective\Luhn\Luhn;


class ServiceProvider extends PackageServiceProvider
{



   // protected $shouldLoadMigrations = true;
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register(){

        $this->app->singleton('kda.phphelpers',function($app){
            
            return new Library\Helper($app);
        });
        $this->app->singleton('kda.phpalgos',function($app){
            
            return new Library\Algo($app);
        });
        $this->app->singleton('luhn',function($app){
            
            return new Luhn();
        });
        $this->app->singleton('modulo11',function($app){
            
            return new Library\Modulo11();
        });
        $this->app->singleton('modulo10',function($app){
            
            return new Library\Modulo10();
        });
    }

    public function bootSelf(){

    }
}
