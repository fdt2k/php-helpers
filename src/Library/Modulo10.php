<?php

declare(strict_types = 1);

namespace  KDA\Helpers\Library;


/**
 * Modulo10 calculator
 */
class Modulo10 implements Calculator
{
    use Traits\AssertionsTrait;

    /**
     * Check if the last digit of number is a valid modulo 10 check digit
     */
    public function isValid(string $number): bool
    {
        return substr($number, -1) === $this->calculateCheckDigit(substr($number, 0, -1) ?: '');
    }

    /**
     * Calculate the modulo 10 check digit for number
     */
    public function calculateCheckDigit(string $number,$weights=[1,2]): string
    {
        $this->assertNumber($number);

        $sum = $this->calculateSum($number,$weights);

        $mod = $sum %10;
        return (string)(10-$mod);
    }

    protected function calculateSum(string $number,$weights=[1,2]): int
    {
        $weight = $weights[0];
        $sum = 0;
        $sum2= 0;
        for ($pos=strlen($number)-1; $pos>=0; $pos--) {
            $tmp = $number[$pos] * $weight;
            $sum2+= $tmp;
            $weight = ($weight == $weights[0]) ? $weights[1] : $weights[0];
        }
        return $sum2;
    }
}
