<?php 
namespace KDA\Helpers\Facades;

use Illuminate\Support\Facades\Facade;


class Algo extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'kda.phpalgos'; }
}